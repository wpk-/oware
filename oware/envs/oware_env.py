"""The Oware game."""
# https://github.com/openai/gym/blob/master/gym/envs/classic_control/acrobot.py
# https://gym.openai.com/docs/
import logging
from typing import Dict, List, Optional, Tuple

import numpy as np

from gym import core, spaces

__copyright__ = 'Copyright 2020, Paul Koppen'
__credits__ = ('Paul Koppen',)
__licence__ = 'BSD 3-Clause'
__author__ = 'Paul Koppen <oware@paulkoppen.com>'

from oware.game import Oware, OwareState, Player, Position, OwareRules, Reward

Observation = Tuple[Player, np.array]

# SOURCE:
# https://github.com/wpk-/oware/blob/master/oware/envs/oware_env.py


class OwareEnv(core.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array', 'ansi'],
        'video.frames_per_second': 15,
    }

    def __init__(self) -> None:
        self.game = game = OwareRules()

        observation_size = game.board_size + game.num_players
        low = np.zeros(observation_size, dtype=np.uint8)
        high = np.ones(observation_size, dtype=np.uint8) * game.seeds_total
        self.observation_space = spaces.Box(low=low, high=high, dtype=np.uint8,
                                            shape=(observation_size,))
        self.action_space = spaces.Discrete(game.player_size)
        self.reward_range = -game.seeds_total, game.seeds_total

        self.score_history = np.zeros((game.num_players, game.num_players))
        self.state: Optional[OwareState] = None

        self.viewer = None
        self.rng = None
        self.seed()

    def _get_obs(self) -> Observation:
        player = self.state.turn
        board = np.roll(self.state.board, -player * self.game.player_size)
        return player, np.append(board, np.roll(self.state.score, -player))

    def close(self) -> None:
        if self.viewer is not None:
            self.viewer.close()
            self.viewer = None

    def reset(self) -> Observation:
        # Does reset generate a random game or the starting position?
        self.state = Oware.new_game()
        return self._get_obs()

    def seed(self, seed: int = None) -> List[int]:
        seq = np.random.SeedSequence(seed)
        self.rng = np.random.default_rng(seq)
        return [seq.entropy]

    def step(self, action: Position) -> Tuple[Observation, Reward, bool, Dict]:
        """Updates the state with the current player's action.
        Returns the next player's observation and reward.
        Also returns a flag indicating the game finished and an empty
        dict.
        """
        # The Env is not responsible for catching cheaters. The app is.
        self.state, rewards, finished = Oware.play(self.state, action)

        delta = -self.score_history[self.state.turn] + self.state.score
        # Competitive reward. Or should this be up to the agent?
        # This way, at least it's compatible with `core.Env.step()`.
        reward = delta[self.state.turn] * 2 - delta.sum()
        self.score_history[self.state.turn] = self.state.score

        return self._get_obs(), reward, finished, {}

    def render(self, mode: str = 'human') -> Optional[np.array]:
        if mode == 'ansi':
            return self.render_ansi()
        else:
            pass

    def render_ansi(self) -> np.array:
        # TODO: extend to multiple players.
        top = '/' + '-' * (3 * self.game.player_size + 6) + '\\\n'
        bot = '\\' + '-' * (3 * self.game.player_size + 6) + '//\n'
        pat = '|' + ' {:2d}' * self.game.player_size + ' |  {:2d} |\n'

        player = self.state.turn
        board = np.roll(self.state.board, -player * self.game.player_size)
        scores = np.roll(self.state.score, -player)

        s = (top +
             pat.format(*board[self.game.player_slice(1)][::-1], scores[1]) +
             pat.format(*board[self.game.player_slice(0)], scores[0]) +
             bot)

        print(s)
        return player, np.array(list(s))

    def render_human(self) -> np.array:
        pass


class OwareEnv_(core.Env):

    def render(self, mode: str = 'human') -> Optional[np.array]:
        """Renders the game state.
        """
        from gym.envs.classic_control import rendering

        if self.viewer is None:
            self.viewer = rendering.Viewer(500, 500)
            self.viewer.set_bounds(-100, 100, -100, 100)
            rnd = np.random.random(12 * 4 * 3)
            rnd = rnd.reshape((-1, 3)) * [1, 1, 0.3] + [-0.5, -0.5, 0.4]
            self._d = rnd

        if self.state is None:
            return None

        board = self.state.board
        board_size = Oware.board_size
        player_size = Oware.player_size

        sz = 200 // player_size

        for y in (0, 1):
            y0 = y * sz - sz
            y1 = y * sz
            for x in range(0, player_size):
                x0 = (x - player_size / 2) * sz
                x1 = (x - player_size / 2) * sz + sz
                self.viewer.draw_polygon(
                    [(x0, y0), (x1, y0), (x1, y1), (x0, y1)],
                    filled=False
                )

        xx = list(range(player_size)) + list(range(player_size - 1, -1, -1))
        yy = [0] * player_size + [1] * player_size
        seeds = [(xx[h], yy[h])
                 for h in range(board_size) for _ in range(board[h])]

        for i, (x, y) in enumerate(seeds):
            x0 = (2 * x + self._d[i, 0] + 1 - player_size) * sz / 2
            y0 = (2 * y + self._d[i, 1] - 1) * sz / 2
            c = self._d[i, 2]
            piece = self.viewer.draw_circle(sz // 10)
            piece.set_color(c, c, 1)
            piece.add_attr(rendering.Transform(translation=(x0, y0)))

        return_rgb_array = mode == 'rgb_array'
        return self.viewer.render(return_rgb_array=return_rgb_array)
