from random import choices, randint, sample
from typing import Iterable, List, NamedTuple, Tuple

__all__ = ('Oware', 'OwareRules', 'OwareState')

Board = Tuple[int, ...]
Player = int
Position = int
Reward = int
Score = Tuple[Reward, ...]


class InvalidAction(Exception):
    pass


class OwareState(NamedTuple):
    board: Board
    score: Score
    turn: Player


class OwareRules:
    def __init__(self, size: int = 6, num_players: int = 2,
                 initial_seeds: int = 4) -> None:
        """Creates an Oware game.
        """
        self.initial_seeds = initial_seeds
        self.num_players = num_players
        self.player_size = size
        self.board_size = size * num_players

    @property
    def seeds_total(self) -> int:
        return self.initial_seeds * self.board_size

    # Rules info methods.

    def final_rewards(self, board: Board) -> List[Reward]:
        """Computes the final rewards for each player.
        """
        return [sum(board[self.player_slice(player)])
                for player in range(self.num_players)]

    def new_game(self) -> OwareState:
        """Returns a new game in starting position.
        """
        board = (self.initial_seeds,) * self.board_size
        score = (0,) * self.num_players
        return OwareState(board, score, turn=0)

    def next_player(self, player: Player) -> int:
        """Returns the next player.
        """
        return (player + 1) % self.num_players

    def owner(self, house: Position) -> Player:
        """Returns the player owning the house.
        """
        return house // self.player_size

    def player_slice(self, player: Player) -> slice:
        """Returns the houses belonging to the given player.
        """
        sz = self.player_size
        start = sz * player
        return slice(start, start + sz)

    def random_game(self, k_seeds: int = None) -> OwareState:
        """Returns a random game with `k_seeds` seeds on the board.
        Note: when `k_seeds` is small, this may take some iterations.
        """
        total_seeds = self.board_size * self.initial_seeds
        in_game = k_seeds or randint(2, total_seeds)
        captured = total_seeds - in_game

        player = 0

        while True:
            board = fixed_sum(in_game, self.board_size)
            score = fixed_sum(captured, self.num_players)
            state = OwareState(tuple(board), tuple(score), player)

            if not self.is_finished(state):
                return state

    # Methods tightly coupled with the game state.

    def is_finished(self, state: OwareState) -> bool:
        """Returns true if the player has no valid moves left.
        Also returns true if there are not enough seeds in play for the
        other players to win.
        """
        scores = sorted(state.score)
        in_play = sum(state.board)

        if scores[-2] + in_play < scores[-1]:
            return True

        return len(self.valid_moves(state)) == 0

    def play(self, state: OwareState, action: Position
             ) -> Tuple[OwareState, Score, bool]:
        """Returns the new state, after playing `action`.
        Also returns the points rewarded for the move, and a boolean to
        signal whether the game has finished.
        """
        action += self.player_slice(state.turn).start

        if state.board[action] == 0:
            raise InvalidAction(f'House {action} is empty.')

        return self._play(state, action)

    def _play(self, state: OwareState, action: Position
              ) -> Tuple[OwareState, Score, bool]:
        """Computes the next state after playing `action`.
        Returns the next state, rewards for all players and a game over
        flag.
        """
        board_size = self.board_size
        board = state.board
        score = state.score
        player = state.turn

        # Distribute seeds.

        count = board[action]
        rounds = count // (board_size - 1)
        rest = count % (board_size - 1)

        last = (action + rest) % board_size

        if last >= action:
            board = [h + rounds for h in board] if rounds else list(board)
            for i in range(action+1, last+1):
                board[i] += 1
        else:
            board = [h + rounds + 1 for h in board]
            for i in range(last+1, action+1):
                board[i] -= 1
        board[action] = 0

        # Capture seeds.

        next_player = self.next_player(player)
        next_slice = self.player_slice(next_player)

        reward = 0

        if self.owner(last) != player:
            no_capture = list(board)
            stop = next_slice.start - 1

            while last != stop and board[last] in (2, 3):
                if last == -1:
                    last += board_size
                reward += board[last]
                board[last] = 0
                last -= 1

            if reward and not any(board[next_slice]):
                board = no_capture
                reward = 0

        # Return values.

        if reward > 0:
            score = list(score)
            score[player] += reward

        next_state = OwareState(tuple(board), tuple(score), next_player)
        finished = self.is_finished(next_state)

        if finished:
            rewards = self.final_rewards(next_state.board)
            score = map(sum, zip(score, rewards))
            board = (0,) * self.board_size
            next_state = OwareState(board, tuple(score), next_player)
        else:
            rewards = [0] * self.num_players
        rewards[player] += reward

        return next_state, tuple(rewards), finished

    def valid_moves(self, state: OwareState) -> List[Position]:
        """Returns the list of valid moves for the given player.
        An empty result means the game is finished.
        """
        board = state.board
        player = state.turn
        player_slice = self.player_slice(player)
        next_slice = self.player_slice(self.next_player(player))
        must_provide = not any(board[next_slice])

        if must_provide:
            h0 = player_slice.start
            h1 = player_slice.stop
            return [h - h0
                    for h, count in enumerate(board[player_slice], start=h0)
                    if h + count >= h1]
        else:
            return [h
                    for h, count in enumerate(board[player_slice])
                    if count > 0]


# The default Oware game uses the default rules.
Oware = OwareRules()


def fixed_sum(total: int, n: int) -> List[int]:
    """Returns `n` random non-negative integers summing to `total`.
    The distribution of returned sequences is uniform.
    """
    # See: https://stackoverflow.com/a/3590105
    def fixed_sum_pos(t: int) -> List[int]:
        dividers = sorted(sample(range(1, t), n - 1))
        return [a - b for a, b in zip(dividers + [t], [0] + dividers)]
    return [x - 1 for x in fixed_sum_pos(total + n)]
