#!/usr/bin/env python
from time import sleep
from typing import Any

import gym

from oware import Oware
from oware.envs import OwareEnv

gym.register('Oware-v0', entry_point=OwareEnv)


class HumanAgent:
    def __init__(self, env: gym.Env) -> None:
        self.ACTIONS = env.action_space.n
        self.env = env
        self.next_action = None
        self.paused = False
        self.restart_requested = False
        self.window_closed = False

        env.render(mode='human')

        env.unwrapped.viewer.window.on_close = self.on_close
        env.unwrapped.viewer.window.on_key_press = self.on_key_press

    def on_close(self) -> None:
        self.window_closed = True

    def on_key_press(self, key: int, mod: Any) -> None:
        if key in (0xff0d, 0xff1b):
            self.restart_requested = True
        elif key == 32:
            self.paused = not self.paused

        if key in range(0xffb0, 0xffc0):
            # Map keypad 0..9 to standard numeric keys.
            key = key - 0xffb0 + ord('0')

        action = int(key - ord('0'))
        if action in Oware.valid_moves(env.state):
            self.next_action = action
        else:
            print(f'Valid actions are: {Oware.valid_moves(env.state)}.')

    def rollout(self) -> None:
        self.restart_requested = False
        self.env.reset()
        self.env.print()

        total_reward = 0
        total_timesteps = 0

        done = False

        while True:
            if self.paused:
                self.env.render(mode='human')
            else:
                if self.next_action is not None:
                    action = self.next_action
                    self.next_action = None

                    total_timesteps += 1

                    obs, reward, done, info = self.env.step(action)
                    if reward != 0:
                        print(f'Reward: {reward:0.3f}')
                    self.env.print()

                    total_reward += reward

                env.render()

                if self.window_closed:
                    return

                if done or self.restart_requested:
                    break
            sleep(0.1)

        print(f'Timesteps: {total_timesteps:i}\nReward: {total_reward:0.2f}')


if __name__ == '__main__':
    env = gym.make('Oware-v0')
    agent = HumanAgent(env)
    # print(f'ACTIONS={agent.ACTIONS}')
    print('Press keys 1 2 3 ... to take actions 1 2 3 ...')

    while True:
        agent.rollout()
        if agent.window_closed:
            break
