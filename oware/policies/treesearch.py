from itertools import count
from random import choice
from typing import Tuple

import numpy as np

from oware import Oware, OwareState
from oware.game import Position


class OwareTreeSearchPolicy:
    def __init__(self, depth) -> None:
        self.depth = depth

    def __call__(self, state: OwareState) -> Position:
        best_actions, expectation = evaluate(state, self.depth)
        action = choice(best_actions)
        print(f'OwareTreeSearchPolicy picks {action} from {best_actions}.')
        return action


def evaluate(state: OwareState, depth: int) -> Tuple[np.array, np.array]:
    """Returns all best moves and the expectation (to all players) over those.
    """
    player = state.turn
    moves = Oware.valid_moves(state)
    valuation = np.zeros((len(moves), Oware.num_players))

    for i, action in enumerate(moves):
        s, valuation[i, player], f = Oware.play(state, action)
        if f:
            valuation[i] += Oware.final_rewards(s.board)
        elif depth > 0:
            valuation[i] += evaluate(s, depth-1)[1]

    max_val = np.amax(valuation[:, player])
    ix_max = valuation[:, player] == max_val

    valuation = valuation[ix_max]
    moves = [moves[i] for i in np.flatnonzero(ix_max)]

    return moves, np.mean(valuation, axis=0)
