from oware import OwareState
from oware.game import Position, Oware


class OwareHumanPolicy:
    def __call__(self, state: OwareState) -> Position:
        print_state(state)
        return int(input('Your move (1-6): ')) - 1


def print_state(state: OwareState) -> None:
    player = state.turn
    player_slice = Oware.player_slice(player)
    player_houses = state.board[player_slice]

    opponent = Oware.next_player(player)
    opponent_slice = Oware.player_slice(opponent)
    opponent_houses = state.board[opponent_slice]

    fmt = '|' + 6 * ' {:2d}' + ' |'

    print('/' + '-' * 19 + '\\')
    print(fmt.format(*opponent_houses[::-1]))
    print(fmt.format(*player_houses))
    print('\\' + '-' * 19 + '/')
