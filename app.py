from random import random
from typing import Callable, Sequence, Tuple, Union, Generic, TypeVar, List, \
    Iterator, Iterable

import gym
import numpy as np
from gym import spaces

from oware.envs import OwareEnv

A = TypeVar('A')
O = TypeVar('O')


class Agent(Generic[O, A]):
    def choose_action(self, obs: O) -> A:
        pass


class QFn(Agent[np.array, int]):
    def __init__(self, observation_space: spaces.Box,
                 action_space: spaces.Discrete) -> None:
        self.alpha = 0.1
        self.gamma = 0.6
        self.epsilon = 0.1

        self.observation_space = observation_space
        self.action_space = action_space

        size_in = int(np.prod(observation_space.shape))
        size_out = int(np.prod(action_space.shape))
        self.operator = Network.build([size_in, size_in + size_out, size_out])

    @property
    def theta(self) -> List[np.array]:
        return self.operator.export_weights()

    @theta.setter
    def theta(self, value: Sequence[np.array]) -> None:
        self.operator.import_weights(value)

    def argmax(self, obs: np.array) -> int:
        vec = self.operator.apply(obs)
        return int(np.argmax(vec))

    def choose_action(self, obs: np.array) -> int:
        return self.argmax(obs)

    def evaluate(self, obs: np.array, action: int) -> float:
        vec = self.operator.apply(obs)
        return vec[action]

    def max(self, obs: np.array) -> float:
        vec = self.operator.apply(obs)
        return np.max(vec)

    def update(self, obs: np.array, action: int, value: float) -> None:
        vec = self.operator.apply(obs)
        vec[action] = value
        self.operator.train(obs, vec)

    def train(self, env: OwareEnv, epochs: int = 100):
        alpha = self.alpha
        gamma = self.gamma
        epsilon = self.epsilon

        for i in range(1, epochs + 1):
            print(f'Epoch {i}.')

            turn, obs = env.reset()
            done = False

            while not done:
                if random() < epsilon:
                    action = env.action_space.sample()
                else:
                    action = self.argmax(obs)

                (turn, obs), reward, done, info = env.step(action)

                # old_value = self.evaluate(obs, action)
                # next_max = self.max(obs)
                #
                # new_value = ((1 - alpha) * old_value +
                #              alpha * (reward + gamma * next_max))
                new_value = reward
                self.update(obs, action, new_value)


class Operator:
    def __init__(self, input_size: int, output_size: int) -> None:
        self.input_size = input_size
        self.output_size = output_size

    def apply(self, inputs: np.array) -> np.array:
        pass

    def train(self, inputs: np.array, target: np.array) -> None:
        pass


class Layer(Operator):
    def __init__(self, input_size: int, output_size: int,
                 activation: str = 'sigmoid', learning_rate: float = 0.1,
                 ) -> None:
        """Creates a neural network layer.
        Size is `(n_variables, n_outputs)`.
        """
        super().__init__(input_size, output_size)
        self.weights = np.random.random((input_size, output_size)) * 2 - 1
        self.bias = np.random.random((1, output_size)) * 2 + 1

        self.activation, self.activation_derivative = ACTIVATION[activation]
        self.learning_rate = learning_rate

    def apply(self, inputs: np.array) -> np.array:
        """Computes the layer output for the given input.
        The input is either an array (row vector) or 2D array of row
        vectors.
        Returns a scalar or column vector.
        """
        return self.activation(inputs @ self.weights + self.bias)

    def train(self, inputs: np.array, target: np.array) -> np.array:
        """Updates the layer to give the desired output (in the limit).
        Inputs are row vectors.
        Targets are row vectors.
        Returns the error gradient.
        """
        inputs = np.atleast_2d(inputs)
        out = self.apply(inputs)
        err = target - out
        delta = err * sigmoid_derivative(out)
        self.weights += inputs.T @ delta
        return delta


class Network(Operator):
    def __init__(self, layers: Sequence[Layer]) -> None:
        input_size = layers[0].input_size
        output_size = layers[-1].output_size
        super().__init__(input_size, output_size)
        self.layers = list(layers)

    @property
    def learning_rate(self) -> List[float]:
        return [layer.learning_rate for layer in self.layers]

    @learning_rate.setter
    def learning_rate(self, rates: Iterable[float]) -> None:
        for layer, rate in zip(self.layers, rates):
            layer.learning_rate = rate

    def apply(self, inputs: np.array) -> np.array:
        """Computes the network output(s) for the given input(s).
        """
        out = np.atleast_2d(inputs)
        for layer in self.layers:
            out = layer.apply(out)
        return out

    @classmethod
    def build(cls, sizes: Sequence[int], activation: str = 'sigmoid',
              learning_rate: float = 0.1) -> 'Network':
        """Easy way to build a network.
        Just pass layer sizes and optional params.
        """
        layers = [Layer(sizes[i], sizes[i+1], activation=activation,
                        learning_rate=learning_rate)
                  for i in range(len(sizes) - 1)]
        return cls(layers)

    def export_weights(self) -> List[np.array]:
        return [(layer.weights, layer.bias) for layer in self.layers]

    def import_weights(self, values: Sequence[np.array]) -> None:
        for layer, (weights, bias) in zip(self.layers, values):
            layer.weights = (layer.weights * 0) + weights
            layer.bias = (layer.bias * 0) + bias

    def train(self, inputs: np.array, target: np.array) -> None:
        """Trains the network with given inputs and target values.
        """
        def train_layer(iter_layers: Iterator[Layer], layer_input: np.array
                        ) -> np.array:
            # We could actually forward-pass the activation derivative.
            # That would result in an entirely different setup, more
            # like all serious neural network libraries do.
            try:
                layer = next(iter_layers)

            except StopIteration:
                # I don't think this is the "error" really. More the gradient
                # of the MSE. People call it the error. OK, fine.
                err = target - layer_input

            else:
                out = layer.apply(layer_input)
                err = train_layer(iter_layers, out)
                delta = err * layer.activation_derivative(out)
                err = delta @ layer.weights.T

                # Note: the cell sum in `layer_input.T @ delta` is dependent
                #       on their size!
                layer.weights += layer.learning_rate * layer_input.T @ delta
                layer.bias += layer.learning_rate * delta.sum(axis=0)

            return err

        inputs = np.atleast_2d(inputs)
        target = np.atleast_2d(target)
        return train_layer(iter(self.layers), inputs)


def sigmoid(x: np.array) -> np.array:
    """The sigmoid function.
    """
    return 1 / (1 + np.exp(-np.asarray(x)))


def sigmoid_derivative(x: np.array) -> np.array:
    """Derivative of the sigmoid function.
    """
    return x * (1 - x)


def tanh(x: np.array) -> np.array:
    """The `tanh` activation function (nonlinearity).
    """
    return np.tanh(x)


def tanh_derivative(x: np.array) -> np.array:
    """Returns the `tanh` derivative at `x`.
    """
    return 1.0 - x ** 2


ACTIVATION = {
    'sigmoid': (sigmoid, sigmoid_derivative),
    'tanh': (tanh, tanh_derivative),
}


def mse(x: np.array, y: np.array) -> float:
    return np.sum((x - y) ** 2) / x.shape[0]


def one_hot(vec: np.array) -> np.array:
    """Returns the one-hot vector encoding for integer labels.
    """
    sz = vec.size
    enc = np.zeros((sz, vec.max() + 1))
    enc[np.arange(sz), vec] = 1
    return enc


if __name__ == '__main__':
    gym.register('Oware-v0', entry_point='oware.envs:OwareEnv')
    env = gym.make('Oware-v0')

    from oware import Oware

    agent = QFn(env.observation_space, env.action_space)
    players = [agent, agent]
    finished = False

    turn, observation = env.reset()

    while not finished:
        action = players[turn].choose_action(observation)
        (turn, observation), reward, finished, _ = env.step(action)
        if reward:
            print(f'Player {turn+1} gains {reward} points.')
        if finished:
            break

    score = list(map(sum, zip(state.score, Oware.final_rewards(state.board))))

    print(f'Final score: {score}')

    if score[0] > score[1]:
        print('** you win!! **')
    elif score[0] == score[1]:
        print('** it\'s a draw! **')
    else:
        print('** you lose. **')


# def rollout(env: OwareEnv, agents: Sequence[Policy]) -> Tuple[int, ...]:
#     """Plays a game to the end.
#     Returns the end evaluation for all players.
#     """
#     max_rounds = 100 * env.game.num_players
#
#     turn, observation = env.reset()
#     score_0 = env.state.score
#
#     for r in range(max_rounds):
#         if turn == 0:
#             env.render(mode='ansi')
#
#         action = agents[turn](observation)
#         (turn, observation), _, done, _ = env.step(action)
#
#         if done:
#             break
#
#     # Competitive evaluation.
#     rewards = np.asarray(env.state.score) - score_0
#     return env.game.num_players * rewards - rewards.sum()
